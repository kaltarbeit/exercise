package com.kaltarbeit.exercise;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by smin on 12/06/2017.
 */
public class DateExercise {

    private int year;
    private int month;
    private int day;
    private String dateString;
    private String today;
    private String aft10Days;
    private String aft12Hours;
    private String opDate;
    private String changedDate;
    private int opNum;
    private int opNum2;
    private String changedTime;

    public String getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(String changedTime) {
        this.changedTime = changedTime;
    }

    public int getOpNum2() {
        return opNum2;
    }

    public void setOpNum2(int opNum2) {
        this.opNum2 = opNum2;
    }

    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }

    private String opTime;

    public int getOpNum() {
        return opNum;
    }

    public void setOpNum(int opNum) {
        this.opNum = opNum;
    }

    public String getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(String changedDate) {
        this.changedDate = changedDate;
    }

    public String getOpDate() {
        return opDate;
    }

    public void setOpDate(String opDate) {
        this.opDate = opDate;
    }

    public String getAft12Hours() {
        return aft12Hours;
    }

    public void setAft12Hours(String aft12Hours) {
        this.aft12Hours = aft12Hours;
    }

    public String getAft10Days() {
        return aft10Days;
    }

    public void setAft10Days(String aft10Days) {
        this.aft10Days = aft10Days;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    Date date = new Date();
    Calendar cal = Calendar.getInstance();

    public void stringToInt() {

        // 1982-12-21

        String[] _tmp = dateString.split("-");

        year = Integer.valueOf(_tmp[0]);
        month = Integer.valueOf(_tmp[1]);
        day = Integer.valueOf(_tmp[2]);
        //   Integer.parseInt(_tmp);
//       java.text.SimpleDateFormat sdf = new  java.text.SimpleDateFormat("");
//        Date to=sdf.parse(dateString);
//     int a = Integer.parseInt(dateString);
//        System.out.println(a);
    }

    public void getCurrentDate() {
        today = sdf.format(date);
        //   System.out.println(today);
    }

    public void changeDay() {
        cal.add(Calendar.DATE, 10);
        aft10Days = sdf.format(cal.getTime());
        cal.add(Calendar.HOUR, 12);
        aft12Hours = sdf.format(cal.getTime());
    }

    //
//    public void changeHour() {
//
//    }
    public String changeOptionalDate(int opNum) {
        SimpleDateFormat changeToDate = new SimpleDateFormat("yyyy-MM-dd");

        String[] _tmp = opDate.split("-");
        cal.set(Integer.parseInt(_tmp[0]), Integer.parseInt(_tmp[1]) - 1, Integer.parseInt(_tmp[2]));
        cal.add(Calendar.DATE, -opNum);
        changedDate = changeToDate.format(cal.getTime());
        return changedDate;
    }

    public String changeOptionalDate() {
        return changeOptionalDate(10);
    }

    public String changeOptionalTime(int opNum2) {
        SimpleDateFormat changeToTime = new SimpleDateFormat("kk:mm:ss");
        String[] _tmpArr = opTime.split(":");
        cal.set(0, 0, 0, Integer.parseInt(_tmpArr[0]), Integer.parseInt(_tmpArr[1]), Integer.parseInt(_tmpArr[2]));
        cal.add(Calendar.HOUR_OF_DAY, opNum2);
        changedTime = changeToTime.format(cal.getTime());
        return changedTime;
    }

    public String changeOptionalTime() {
        return changeOptionalTime(12);
    }

}
