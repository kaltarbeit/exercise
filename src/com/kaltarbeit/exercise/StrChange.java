package com.kaltarbeit.exercise;

/**
 * Created by smin on 12/06/2017.
 */
public class StrChange {

    private String up;
    private String low;

    private String strX;

    public String getStrX() {
        return strX;
    }

    public void setStrX(String strX) {
        this.strX = strX;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String changeToUp() {

        return low.toUpperCase();
    }
    public String changeToLow(){

    return up.toLowerCase();
    }

    public String changeToX(){

        return strX.replaceAll("X", "엑스");
    }
}
