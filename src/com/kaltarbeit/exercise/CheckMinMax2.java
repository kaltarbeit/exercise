package com.kaltarbeit.exercise;

/**
 * Created by smin on 09/06/2017.
 */
public class CheckMinMax2 {
    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    private int[] arr;
    private int max;
    private int min;



    public void doGetMax(){
        int base=arr[0];
        for(int i=0;i<arr.length;i++){
            if(base<arr[i]){
                base=arr[i];
            }
        }
        max=base;
    }
    public void doGetMin(){
        int base=arr[0];
        for(int i=0;i<arr.length;i++){
            if(base>arr[i]){
                base=arr[i];
            }
        }
        min=base;
    }

}
