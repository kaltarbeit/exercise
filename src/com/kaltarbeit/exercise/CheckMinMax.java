package com.kaltarbeit.exercise;

import java.util.Arrays;

/**
 * Created by smin on 08/06/2017.
 */
public class CheckMinMax {

    private int min;
    private int max;
    private int[] arr;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    private int result;

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }


    public void doGetMax() {

        int _tmp = arr[0];

        for (int i = 0; i < arr.length; i++) {
            if (_tmp < arr[i]) {
                _tmp = arr[i];
            }
        }
        result = _tmp;
    }
    public void doGetMin() {

        int tmp = arr[0];
        for (int i = 0; i < arr.length - 1; i++) {
            if (tmp > arr[i]) {

                tmp = arr[i];

            }
        }
        result = tmp;
    }
}
