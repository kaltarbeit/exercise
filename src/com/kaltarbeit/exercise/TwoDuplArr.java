package com.kaltarbeit.exercise;

import java.util.Arrays;


/**
 * Created by smin on 11/06/2017.
 */
public class TwoDuplArr {

    private int[] numArr1;
    private int[] numArr2;
    private int[] newNumArr;

    private String[] strArr1;
    private String[] strArr2;
    private String[] newStrArr;

    public int[] getNumArr1() {
        return numArr1;
    }

    public void setNumArr1(int[] numArr1) {
        this.numArr1 = numArr1;
    }

    public int[] getNumArr2() {
        return numArr2;
    }

    public void setNumArr2(int[] numArr2) {
        this.numArr2 = numArr2;
    }

    public int[] getNewNumArr() {
        return newNumArr;
    }
    public void setNewNumArr(int[] newNumArr) {
        this.newNumArr = newNumArr;
    }

    public String[] getStrArr1() {
        return strArr1;
    }

    public void setStrArr1(String[] strArr1) {
        this.strArr1 = strArr1;
    }

    public String[] getStrArr2() {
        return strArr2;
    }

    public void setStrArr2(String[] strArr2) {
        this.strArr2 = strArr2;
    }

    public String[] getNewStrArr() {
        return newStrArr;
    }

    public void setNewStrArr(String[] newStrArr) {
        this.newStrArr = newStrArr;
    }




    public int[] doGetTwoDuplNumArr() {
        int count = 0; // 배열 -시키는 카운팅
        int t = 0; //새로운 배열 길이
        int[] num = new int[numArr1.length];// 초기화

        for (int i = 0; i < numArr2.length; i++) {
//            if(num[i-1]==num[i]){
//            for(int a=0;a<num.length;a++){
//                    num[num.length-a]=num[i];
//                }
//            }
            for (int j = 0; j < i; j++) {
                if (numArr2[j] == numArr1[i]) {
                    num[i] = numArr2[j];
                    //    setNewNumArr(num);
                    if (num[i] > 0) {
                        num[t] = num[i];
                        t++;
                    }
                    //  System.out.println(Arrays.toString(num));
                }
            }
            if (num[i] == 0) {
                count++;
            }
        }
//        for(int a=i-1;a>num.length;i--) {
//            if (num[a] == num[i]) {
//
//                num[a] = num[i];
//            }
//        }
        return Arrays.copyOf(num, num.length - count);
    }

    public String[] doGetTwoDuplStrArr() {
        int t = 0;
        int count = 0;

        String[] str = new String[strArr1.length];
        for (int i = 0; i < strArr1.length; i++) {
            for (int j = 0; j < strArr2.length; j++) {

                if (strArr1[i].equals(strArr2[j])) {

                    str[i] = strArr1[i];

                    if (str[i] != null) {
                        str[t] = str[i];
                        t++;
                        count=str.length-t;
                    }
                    //setNewStrArr(str);
                    //      }
                }
//                if (str[t] == null) {
//                    count++;
//                }
                //  System.out.println(Arrays.toString(str));
               // count ++ ;
            }
        }
        return Arrays.copyOf(str, str.length - count);
    }
}
