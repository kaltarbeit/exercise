package com.kaltarbeit.exercise;

import java.util.Scanner;

/**
 * Created by smin on 08/06/2017.
 */
public class Baseball {

    public static void main(String args[]) {

        int cst = (int) (Math.random() * 10);
        int cnd = (int) (Math.random() * 10);
        int crd = (int) (Math.random() * 10);
        int[] answer = {cst, cnd, crd};

        System.out.println(cst + "," + cnd + "," + crd);


        int ball, strike;
        int turn = 0;

        do {

            ball = 0;
            strike = 0;
            Scanner scanner = new Scanner(System.in);
            System.out.println("첫번째 자리수를 입력해주세요");
            int st = scanner.nextInt();
            System.out.println("두번째 자리수를 입력해주세요");
            int nd = scanner.nextInt();
            System.out.println("세번째 자리수를 입력해주세요");
            int rd = scanner.nextInt();
            int[] guess = {st, nd, rd};

            for (int i = 0; i < answer.length; i++) {
                for (int j = 0; j < guess.length; j++) {
                    if (answer[i] == guess[j] && i != j) {
                        // System.out.println("ball");
                        ball++;
                    } else if (answer[i] == guess[j] && i == j) {
                        // System.out.println("strike");
                        strike++;
                    } else {
                        // System.out.println("again");
                    }
                }
            }
            System.out.println("//////////////////");
            if (strike == 3) {
                System.out.println("success!!!");
            } else {
                System.out.println("ball:" + ball + "," + "strike:" + strike);
                turn++;
            }

        } while (strike != 3 && turn < 5);
        if (turn == 5) {
            System.out.println("fail!!");
        }

        //숫자가 맞으면 ball
        //자리까지 맞으면 strike
        //다 맞으면 success
    }

}
