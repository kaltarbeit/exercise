package com.kaltarbeit.exercise;

import com.kaltarbeit.exercise.exceptions.TriangleException;

/**
 * Created by smin on 08/06/2017.
 */
public class Triangle {

    private int a,b,c;  // 세 변
    private int width, height;  // 밑변 , 높이
    private double area; // 면적

    public Triangle() {
        System.out.println("Triangle 이 생성되었습니다 .");
    }

    public Triangle(int width, int height) {
        this.width=width;
        this.height=height;
    }

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getA() {
        return this.a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public double getArea() {
        return this.area;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void calculatAreaWithWidthAndHeight(){

        this.area = this.width * this.height / 2;
    }

    public void calculatAreaWithTreeLine() throws Exception {

        if(a == 0 ||b==0 || c==0) {
            throw new TriangleException();
//            throw new TriangleException("세 변의 길이가 옳지 않습니다 .");
        }

        double s = (a+b+c)/2;

        double ss = s*(s-a)*(s-b)*(s-c);

        this.area= Math.sqrt(ss);

    }

}
