package com.kaltarbeit.exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by smin on 10/06/2017.
 */
public class DuplArray {

    private int[] numArr;
    private int[] newNumArr;
    private String[] strArr;
    private String[] newStrArr;

    public String[] getNewStrArr() {
        return newStrArr;
    }

    public void setNewStrArr(String[] newStrArr) {
        this.newStrArr = newStrArr;
    }


    public String[] getStrArr() {
        return strArr;
    }

    public void setStrArr(String[] strArr) {
        this.strArr = strArr;
    }

    public int[] getNumArr() {
        return numArr;
    }

    public void setNumArr(int[] numArr) {
        this.numArr = numArr;
    }

    public int[] getNewNumArr() {
        return newNumArr;
    }

    public void setNewNumArr(int[] newNumArr) {
        this.newNumArr = newNumArr;
    }


    public int[] getDuplNumArr() {

        List<Integer> _returnList = new ArrayList<Integer>();


        for (int i = 0; i < numArr.length;i++) {

//            int _cur = numArr[i];

            for(int j = i+1; j < numArr.length; j++) {

                if(numArr[i] == numArr[j]) {

                    if(!_returnList.contains(numArr[i])) {
                        _returnList.add(numArr[i]);

                    }

                }

            }
        }

        return listMeth(_returnList);
    }

    public int[] listMeth(List<Integer>_returnList){

        int[] _returnArray = new int[_returnList.size()];

        int i = 0;

        for(Integer _i : _returnList) {

            _returnArray[i++] = _i.intValue();

        }
        return _returnArray;
    }

    public String[] getDuplStrArr() {
     //   int count = 0;
        int turn = 0;//배열 앞에 입력 시킬 주소
        int j = 0; // '-' 카운팅

        String[] s = new String[strArr.length];
        Arrays.sort(strArr);
        //    System.out.println(Arrays.toString(strArr));

        for (int i = 0; i < strArr.length - 1; i++) {// 배열을 돌려.

            if (strArr[i].equals(strArr[i + 1])) {
              //  count++;
               // if (count == 1) {
                    s[turn] = strArr[i];

                    if (s[turn] != null) {
                        turn++;
                        //   setNewStrArr(Arrays.copyOf(s,s.length-turn));
                        j = s.length - i;
                    }
                }
             //   count--;
          //  }
        }
        return Arrays.copyOf(s, s.length - j - 1);
    }
}


//            return Arrays.copyOf(n, n.length - 1);
//  for (int b = 0;b < n.length; b++) {
//            if (n[i] == 0) {
//
//                 for (int c = i; c < n.length - 1; c++) {
//                    n[c] = n[c + 1];
//
//
//                }
//            }
//  System.out.println(Arrays.toString(n));
//   return Arrays.copyOf(n, n.length - 1);




