package com.kaltarbeit.exercise;

/**
 * Created by smin on 08/06/2017.
 */
public class LeapYear {

    private int year;
    private String result;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public LeapYear() {

    }

    public void doGetLeapYear() {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) { //4로 나누어 떨어지지만 100으로는 안나누어 떨어지는 해 또는 400으로 나누어 떨어지는해
           this.result="윤년";
        } else if (year % 4 == 0 && year % 100 == 0 || year%400!=0) {//4로 나누어 떨어지지만 100으로도 나누어 떨어지는 해 또는 400으로 x
           this.result="평년";
        }else {
           this.result="평년";
        }
    }
}
