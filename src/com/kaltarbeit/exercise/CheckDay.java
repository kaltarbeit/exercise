package com.kaltarbeit.exercise;

import com.kaltarbeit.exercise.exceptions.DayException;

/**
 * Created by smin on 08/06/2017.
 */
public class CheckDay {

    private static final String MON = "월";

    public CheckDay() {

    }

    private int num;
    private String day;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void checkedDay() throws Exception {

        this.checkedDay(this.num);
    }

    public void checkedDay(int num) throws Exception {

        if (0 > num || num > 7) {
            throw new DayException();
        }

        switch (num) {
            case 1:
                this.day = MON;
                break;
            case 2:
                this.day = "월";
                break;
            case 3:
                this.day = "화";
                break;
            case 4:
                this.day = "수";
                break;
            case 5:
                this.day = "목";
                break;
            case 6:
                this.day = "금";
                break;
            case 7:
                this.day = "토";
                break;

        }

    }




}
