package com.kaltarbeit.exercise;

import java.util.Arrays;

/**
 * Created by smin on 12/06/2017.
 */
public class StrMatch {

    private String str1;
    private String str2;

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    private String[] data1Arr;
    private String[] data2Arr;

    public int matchStrCount() {

       data1Arr=buildStringToArray(str1,2);
       data2Arr=buildStringToArray(str2,2);

        int count = 0;
        for (int i = 0; i < data1Arr.length; i++) {
            for (int j = i; j < data2Arr.length; j++) {
                if (data1Arr[i].equals(data2Arr[j])) {
                    count++;
                }
            }
        }
//        System.out.println(count);
        return count;
    }

    public String[] buildStringToArray(String str) {
        return buildStringToArray(str, 2);
    }

    public String[] buildStringToArray(String str, int size) {

        String[] dataArr = new String[str.length() - size + 1];

        for (int i = 0; i < str.length() - size + 1; i++) {
            String _data = str.substring(i, i + size);
            dataArr[i] = _data;
        }

        return dataArr;
    }

}
