package com.kaltarbeit.exercise;

/**
 * Created by smin on 09/06/2017.
 */
public class LeapYear2 {

    private int year;
    private String result;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



    public void doGetLeapYear(){

        if(year%4==0 && year%100 !=0 || year%400==0){
            result="윤년";
        }else if(year%4==0 && year%100==0||year%400!=0){
            result="평년";
        }else{
            result="평년";
        }
    }
}
