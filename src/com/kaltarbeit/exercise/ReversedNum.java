package com.kaltarbeit.exercise;

import java.util.Arrays;

/**
 * Created by smin on 09/06/2017.
 */
public class ReversedNum {
    public int[] getNumArr() {
        return numArr;
    }

    public void setNumArr(int[] numArr) {
        this.numArr = numArr;
    }

    private int[] numArr;

    public int[] reverseNum() {

        int b;

//
        int[] vv = new int[numArr.length];
        for (int i = 0; i < numArr.length; i++) {

            b = numArr[i];
            vv[(numArr.length - 1) - i] = b;

        }

//        System.out.println(Arrays.toString(vv));
        return vv;
    }

}
