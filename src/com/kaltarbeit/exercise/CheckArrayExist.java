package com.kaltarbeit.exercise;

import java.util.Arrays;
import java.util.List;

/**
 * Created by smin on 08/06/2017.
 */


public class CheckArrayExist {

    private int[] arry;
    private int num;
    private boolean result;

    public int[] getArry() {
        return arry;
    }

    public void setArry(int[] arry) {
        this.arry = arry;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public CheckArrayExist() {

    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public boolean checkExist() {

        for (int i = 0; i < arry.length; i++) {
            if (arry[i] == num) {
                this.result = true;
            }
        }
        return result;

    }

    public int[] deleteExist() {

        int i;
        for (i = 0; i < arry.length; i++) {
            if (arry[i] == num) {

                for(int j = i; j < arry.length -1 ; j++) {
                    arry[j] = arry[j + 1];
                }
            }
        }

        return Arrays.copyOf(arry, arry.length -1);
    }
}

