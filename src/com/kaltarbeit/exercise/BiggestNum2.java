package com.kaltarbeit.exercise;

/**
 * Created by smin on 09/06/2017.
 */
public class BiggestNum2 {

    private int st;
    private int nd;
    private int rd;

    private int result;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getSt() {
        return st;
    }

    public void setSt(int st) {
        this.st = st;
    }

    public int getNd() {
        return nd;
    }

    public void setNd(int nd) {
        this.nd = nd;
    }

    public int getRd() {
        return rd;
    }

    public void setRd(int rd) {
        this.rd = rd;
    }

    public void compareNum(){
        result=getSt();
        if(st<nd){
            result=getNd();
        }else if(nd<rd){
            result=getRd();
        }
    }


}
