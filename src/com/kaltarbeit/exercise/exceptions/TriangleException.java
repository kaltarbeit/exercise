package com.kaltarbeit.exercise.exceptions;

/**
 * Created by smin on 08/06/2017.
 */
public class TriangleException extends Exception {

    public TriangleException() {
        super("세 변의 길이 혹은 밑변 , 높이가 옳지 않습니다.");
    }

    public TriangleException(String msg) {
        super(msg);
    }


}
