package com.kaltarbeit.exercise.exceptions;

/**
 * Created by smin on 08/06/2017.
 */
public class DayException extends Exception {
    public DayException(){
        super("숫자가 맞지않습니다.");
    }
}
