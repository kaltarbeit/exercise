package com.kaltarbeit.exercise;

/**
 * Created by smin on 09/06/2017.
 */
public class CheckDay2 {

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    private String day;
    private Week week;


    public enum Week {
        SUN("1"), MON("2"), TUE("3"), WED("4"), THU("5"), FRI("6"), SAT("7");

        private String label;

        Week(String label){
            this.label = label;
        }

        public String getLabel(){
            return this.label;
        }
    }

    public void doGetDay(){
        Week.valueOf("일");
//        Week.FRI.getLabel();
//        Week.SUN.name();
    }
}
