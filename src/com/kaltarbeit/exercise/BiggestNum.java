package com.kaltarbeit.exercise;


/**
 * Created by smin on 08/06/2017.
 */
public class BiggestNum {

    private int st;//첫번째
    private int nd;//두번째
    private int rd;//세번째

    private int result; //결과(가장큰값)

    public BiggestNum(){

    }

    public void setSt(int st){
        this.st=st;
    }

    public int getSt(){
        return st;
    }

    public void setNd(int nd){
        this.nd=nd;
    }

    public int getNd(){
        return nd;
    }

    public void setRd(int rd){
        this.rd=rd;
    }

    public int getRd(){
        return rd;
    }

    public void setResult(int result){
        this.result=result;
    }

    public int getResult(){
        return result;
    }

    public void compareNum(){
        result=st;
        if(st<nd){
            result=nd;
        }
        if(nd<rd){
            result=rd;
        }
    }

}
